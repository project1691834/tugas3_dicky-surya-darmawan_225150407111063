package com.project.projek2;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView body, beard, eyebrow, eyes, hair, moustache;
    private CheckBox checkBox, checkBox2, checkBox3, checkBox4, checkBox5, checkBox6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        body = findViewById(R.id.body);
        checkBox = findViewById(R.id.checkBox);
        beard = findViewById(R.id.beard);
        checkBox2 = findViewById(R.id.checkBox2);
        eyebrow = findViewById(R.id.eyebrow);
        checkBox3 = findViewById(R.id.checkBox3);
        eyes = findViewById(R.id.eyes);
        checkBox4 = findViewById(R.id.checkBox4);
        hair = findViewById(R.id.hair);
        checkBox5 = findViewById(R.id.checkBox5);
        moustache = findViewById(R.id.moustache);
        checkBox6 = findViewById(R.id.checkBox6);

        body.setVisibility(View.INVISIBLE);
        beard.setVisibility(View.INVISIBLE);
        eyebrow.setVisibility(View.INVISIBLE);
        eyes.setVisibility(View.INVISIBLE);
        hair.setVisibility(View.INVISIBLE);
        moustache.setVisibility(View.INVISIBLE);

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    body.setVisibility(View.VISIBLE);
                } else {
                    body.setVisibility(View.INVISIBLE);
                }
            }
        });

        checkBox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox2.isChecked()) {
                    beard.setVisibility(View.VISIBLE);
                } else {
                    beard.setVisibility(View.INVISIBLE);
                }
            }
        });

        checkBox3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox3.isChecked()) {
                    eyebrow.setVisibility(View.VISIBLE);
                } else {
                    eyebrow.setVisibility(View.INVISIBLE);
                }
            }
        });

        checkBox4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox4.isChecked()) {
                    eyes.setVisibility(View.VISIBLE);
                } else {
                    eyes.setVisibility(View.INVISIBLE);
                }
            }
        });

        checkBox5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox5.isChecked()) {
                    hair.setVisibility(View.VISIBLE);
                } else {
                    hair.setVisibility(View.INVISIBLE);
                }
            }
        });

        checkBox6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox6.isChecked()) {
                    moustache.setVisibility(View.VISIBLE);
                } else {
                    moustache.setVisibility(View.INVISIBLE);
                }
            }
        });

    }


}